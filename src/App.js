import React from 'react';
import BrandSelect from './components/selects/BrandSelect/BrandSelect';
import MarkSelect from './components/selects/MarkSelect/MarkSelect';
import SphSelect from './components/selects/SphSelect/SphSelect';
import CitySelect from './components/selects/CitySelect/CitySelect';
import Table from './components/selects/Table/Table';
import './app.css'

const App = () => {

  return (
    <div className="select">
      <BrandSelect />
      <MarkSelect />
      <SphSelect />
      <CitySelect />
      <Table />
    </div>
  );
}

export default App;
