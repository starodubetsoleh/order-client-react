import React, { useEffect } from "react"
import { connect } from 'react-redux';

import { requestBrands, setBrand, setMark, getMarks, setMarkCode } from "../../../store/actions/actions";
import { getBrands, getBrand } from '../../../store/selectors/brandSelectors';

const BrandSelect = ({ requestBrands, brands, setBrand, setMark, getMarks, setMarkCode }) => {

    useEffect(
        () => {
            requestBrands();
        },
        [requestBrands],
    )

    const handleOnChange = (event) => {
        setMark('');
        getMarks();
        setMarkCode('')
        return setBrand(event.target.value)
    }

    return (
        <select
            onChange={handleOnChange}
            className='brand-select'
        >
            <option hidden value="0">Take Brand</option>
            {brands.map((brand) => (
                <option
                    value={brand}
                    key={brand}
                >
                    {brand}
                </option>
            ))}
        </select>
    );
}

const mapStateToProps = (state) => {
    return {
        brands: getBrands(state),
        brand: getBrand(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        requestBrands: () => dispatch(requestBrands),
        setBrand: (brand) => dispatch(setBrand(brand)),
        setMark: (mark) => dispatch(setMark(mark)),
        getMarks: () => dispatch(getMarks),
        setMarkCode: (code) => dispatch(setMarkCode(code)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BrandSelect);