import React from "react"
import { connect } from 'react-redux';

import { setSph } from "../../../store/actions/actions";

const MarkSelect = ({ setSph }) => {

    const handleOnChange = (event) => {
        setSph(event.target.value)
    }

    return (
        <select
            onChange={handleOnChange}
            className='sph-select'
        >
            <option hidden value="0" >SPH +/-</option>
            <option value="-">SPH -</option>
            <option value="+">SPH +</option>
        </select>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSph: (sph) => dispatch(setSph(sph)),
    }
}

export default connect(undefined, mapDispatchToProps)(MarkSelect);