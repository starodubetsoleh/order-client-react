import React from "react"
import { connect } from 'react-redux';

import { setMark, getMarks, setMarkCode } from "../../../store/actions/actions";
import { getBrand, getMarksAll, getMark } from '../../../store/selectors/brandSelectors';


const MarkSelect = ({ setMark, marks, mark, brand, setMarkCode }) => {

    let brandName = [];
    for (let index = 0; index < marks.length; index++) {
        if (marks[index].NameGroup.includes(brand)) {
            brandName.push(marks[index].NameGroup)
        }
    }

    const handleOnChange = (event) => {
        setMark(event.target.value);
        for (let index = 0; index < marks.length; index++) {
            if (marks[index].NameGroup === event.target.value) {
                setMarkCode(marks[index].CodeGroup);
            }
        }
    }

    return (
        <select 
            onChange={handleOnChange}
            value={mark}
            className='mark-select'
        >
            <option hidden value="0">Take Mark</option>
            {brandName.map((mark, index) => (
                <option
                    value={mark}
                    key={index}
                >
                    {mark}
                </option>
            ))}
        </select>
    );
}

const mapStateToProps = (state) => {
    return {
        marks: getMarksAll(state),
        brand: getBrand(state),
        mark: getMark(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMarks: () => dispatch(getMarks),
        setMark: (mark) => dispatch(setMark(mark)),
        setMarkCode: (code) => dispatch(setMarkCode(code)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MarkSelect);