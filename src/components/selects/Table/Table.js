import React, { useEffect, useMemo } from 'react';
import { connect } from 'react-redux';

import { getTableData } from "../../../store/actions/actions";
import { getCityCode, getMarkCode, getPreparedTableData } from "../../../store/selectors/brandSelectors";
import TableView from './TableView';

import './tableRows.css'


const Table = ({ cityCode, markCode, getTableData, preparedTableData =[] }) => {

    useEffect(() => {
        if (cityCode.length > 0 && markCode.length > 0 && cityCode === 'all') {
            getTableData(markCode, '01')
        } else if (cityCode.length > 0 && markCode.length > 0) {
            getTableData(markCode, cityCode)
        }
    }, [markCode, cityCode, getTableData])

    const sortData = useMemo(
        () => preparedTableData.reduce(
            (acc, value) => {
                if (value.Sfera <= 0) {
                    acc.unshift(value)
                } else if (value.Sfera > 0) {
                    acc.push(value)
                }
                return acc;
            }, []
        ),
        [preparedTableData]
    )

    const sortDataView = useMemo(
        () => sortData.map(
            (value) => {
                if(value.Sfera.length === 1 && value.Sfera !== '0'){
                    value.SferaView = `+${value.Sfera}.00`;
                }else if(value.Sfera.length === 3 && value.Sfera.indexOf('-') === 0){
                    value.SferaView = `${value.Sfera}.00`;
                } else if(value.Sfera.length === 2 ){
                    value.SferaView = `${value.Sfera}.00`;
                } else if(value.Sfera.length === 3){
                    value.SferaView = `+${value.Sfera}0`;
                } else if(value.Sfera.length === 4 && value.Sfera.indexOf('-') === 0 ){
                    value.SferaView = `${value.Sfera}0`;
                } else if(value.Sfera.length === 4 && value.Sfera.indexOf('-') === -1 ){
                    value.SferaView = `+${value.Sfera}`;
                } else if(value.Sfera === '0' ){
                    value.SferaView = `-${value.Sfera}.00`;
                } else {
                    value.SferaView = `${value.Sfera}`;
                }

                if(value.Cylinder.length === 2){
                    value.CylinderView = `${value.Cylinder}.00`
                } else if(value.Cylinder.length === 4){
                    value.CylinderView = `${value.Cylinder}0`
                } else if(value.Cylinder === '0'){
                    value.CylinderView = `-${value.Cylinder}.00`
                } else {
                    value.CylinderView = value.Cylinder
                }

                return {
                    Sfera: value.Sfera,
                    Cylinder: value.Cylinder,
                    SferaView: value.SferaView,
                    CylinderView: value.CylinderView,
                    Diameter: value.Diameter,
                    QtyCity: value.QtyCity,
                    QtyAll: value.QtyAll,
                    CodeGroup: value.CodeGroup,
                    Color: value.Color,
                };
            },
        ),
        [sortData]
    )

    return (
        <TableView dataView = {sortDataView} />
    )

}

const mapStateToProps = (state) => {
    return {
        cityCode: getCityCode(state),
        markCode: getMarkCode(state),
        preparedTableData: getPreparedTableData(state),
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        getTableData: (markCode, cityCode) => dispatch(getTableData(markCode, cityCode)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table);