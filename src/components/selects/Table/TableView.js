import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import { getSelectedSph } from "../../../store/selectors/brandSelectors";
import './tableRows.css'
import TableRow from './TableRow';

const TableView = ({ dataView = [], selectedSph }) => {
    const cylView = useMemo(
        () => (
            dataView.reduce((acc, item) => (
                acc.includes(item.CylinderView) ? acc : [...acc, item.CylinderView]
            ), []).sort((a, b) => b - a )
        ), [dataView]
    )
    const sphView = useMemo(
        () => (
            dataView.reduce((acc, item) => (
                acc.includes(item.SferaView) ? acc : [...acc, item.SferaView]
            ), [])
        ), [dataView]
    )

    const dataObj = useMemo(() => {
            let obj = {}
            for (let i = 0; i < sphView.length; i++) {
                let arr = [];
                for (let j = 0; j < dataView.length; j++) {
                    if(dataView[j].SferaView === sphView[i]){
                        arr.push(dataView[j])
                    }
                }
                obj[i] = arr
            }
            return obj
    }, [dataView, sphView])

    const dataTable = useMemo(
        () => {
            if(selectedSph === '+'){
                return Object.values(dataObj).map( item => {
                    return item.sort((a,b) => (b.Cylinder - a.Cylinder))
                })
            } else {
                return Object.values(dataObj)
            }
        }, [dataObj, selectedSph]
    )

    return (
        <table>
            <tbody>
                <tr>
                    <th>SPH/CYL</th>
                    {cylView.map((item, index) => (
                        <th 
                            key={index}
                        >
                            {item}
                        </th>
                    ))}
                </tr>
                {dataTable.map((item, index) => (
                    <TableRow 
                        key = {index} 
                        data = {item}
                    />
                ))}
            </tbody>
        </table>
    )
}

const mapStateToProps = (state) => {
    return {
        selectedSph: getSelectedSph(state),
    }
};

export default connect(mapStateToProps, undefined)(TableView);