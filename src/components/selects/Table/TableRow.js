import React from 'react';
import { useSelector } from 'react-redux';
import { getCityCode } from "../../../store/selectors/brandSelectors";
import classNames from 'classnames';

import './tableRows.css'

const TableRow = ({data}) => {

    const handleClick = ( e ) => {
        console.log(e)
        console.log(cityCode)
    }

    const cityCode = useSelector(getCityCode);

    const isAllCities = cityCode === 'all';

    return(
        <tr>
            <td>
                {data[0].SferaView}
            </td>
            
            {data.map((item, index) => (
                <td
                    key={index}
                    onClick = { () =>  handleClick(item) }
                    className = {classNames({
                        'cursor-pointer': 'cursor-pointer',
                        black: (isAllCities && Number(item.QtyAll) === 0) || Number(item.QtyCity) === 0,
                        red: (isAllCities && Number(item.QtyAll) === 1) || Number(item.QtyCity) === 1,
                    }
                    )}
                >
                    {`${item.SferaView}/${item.CylinderView} D:${item.Diameter}`}
                </td>
            ))}
        </tr>
    )
}

export default TableRow;