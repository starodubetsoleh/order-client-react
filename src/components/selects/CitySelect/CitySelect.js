import React, { useEffect } from "react"
import { connect } from 'react-redux';

import { getCity, setCityCode } from "../../../store/actions/actions";
import { getCityAll } from '../../../store/selectors/brandSelectors';

const CitySelect = ({ getCity, setCityCode, getCityAll, city }) => {

    useEffect(
        () => {
            getCity();
        },
        [getCity],
    )

    const handleOnChange = (event) => {
        city.forEach(elem => {
            if (elem.label === event.target.value) {
                setCityCode(elem.value)
            }
        });
    }

    return (
        <select
            onChange={handleOnChange}
            className='city-select'
        >
            <option hidden value="0">Take City</option>
            {city.map((c, index) => (
                <option
                    value={c.label}
                    key={index}
                >
                    {c.label}
                </option>
            ))}
        </select>
    );
}

const mapStateToProps = (state) => {
    return {
        city: getCityAll(state),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCity: () => dispatch(getCity),
        setCityCode: (code) => dispatch(setCityCode(code)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CitySelect);