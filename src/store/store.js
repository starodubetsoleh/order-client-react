import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import brandReducer from './reducers/brandsReducer';

const middleware = [thunk];

const allReducers = combineReducers(
    {
        brandReducer
    }
);

const initialState = {
    brandReducer: {
        brands: [],
        marks: [],
        city: [],
        selectedSph: '',
        selectedBrand: '',
        selectedMark: '',
        selectedMarkCode: '',
        selectedCityCode: '',
        tableData: [],
    },
};

const store = createStore(allReducers, initialState, compose(applyMiddleware(...middleware),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

export default store;