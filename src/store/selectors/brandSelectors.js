import { createSelector } from "reselect";

export const getBrands = (state) => state.brandReducer.brands;
export const getMark = (state) => state.brandReducer.selectedMark;
export const getBrand = (state) => state.brandReducer.selectedBrand;
export const getMarksAll = (state) => state.brandReducer.marks;
export const getCityAll = (state) => state.brandReducer.city;
export const getCityCode = (state) => state.brandReducer.selectedCityCode;
export const getMarkCode = (state) => state.brandReducer.selectedMarkCode;
export const getTableData = (state) => state.brandReducer.tableData;
export const getSelectedSph = (state) => state.brandReducer.selectedSph;

export const getPreparedTableData = createSelector(
    getTableData,
    getSelectedSph,
    (data, symbol) => {
        if(symbol === '-'){
            return data.filter(e => e.Sfera <= 0)
        } else if(symbol === '+'){
            return data.filter(e => e.Sfera > 0)
        }
    }
)

// return data.filter(({QtyAll, QtyCity, CodeGroup, Cylinder, Diameter, Sfera}) => {
//     if(Sfera.indexOf(simbol) > -1){
//         return {
//             QtyAll,
//             QtyCity,
//             CodeGroup,
//             Cylinder,
//             Diameter,
//             Sfera
//         }
//     }
// });