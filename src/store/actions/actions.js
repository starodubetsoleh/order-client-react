import axios from "axios";

export const GET_BRANDS = "GET_BRANDS";
export const SET_BRAND = 'SET_BRAND';
export const GET_MARKS = 'GET_MARKS';
export const SET_MARK = 'SET_MARK';
export const SET_MARK_CODE = 'SET_MARK_CODE';
export const SET_SPH = 'SET_SPH';
export const GET_CITY = 'GET_CITY';
export const SET_CITY_CODE = 'SET_CITY_CODE';
export const GET_TABLE_DATA = 'GET_TABLE_DATA';

export const getMarks = (dispatch) => {
    return axios.get('http://localhost:3030/group-list').then(
        (res) => dispatch({
            type: GET_MARKS,
            payload: res.data.GroupItems
        })
    )
}

export const requestBrands = (dispatch) => {
    return axios('http://localhost:3030/brand').then(
        (res) => dispatch({
            type: GET_BRANDS,
            payload: res.data
        })
    )
};

export const setBrand = (payload) => {
    return ({
        type: SET_BRAND,
        payload
    })
};

export const getCity = (dispatch) => {
    return axios.get(`http://localhost:3030/city`).then(
        (res) => dispatch({
            type: GET_CITY,
            payload: res.data
        })
    )
};
export const getTableData = (markCode, cityCode) => (dispatch) => {
    return axios.get(`http://localhost:3030/ostatki?CodeGroup=${markCode}&CodeCity=${cityCode}`).then(
        (res) => dispatch({
            type: GET_TABLE_DATA,
            payload: res.data
        })
    ).catch( err => console.error(err))
};

export const setMark = (payload) => {
    return ({
        type: SET_MARK,
        payload
    })
};

export const setMarkCode = (payload) => {
    return ({
        type: SET_MARK_CODE,
        payload
    })
};
export const setSph = (payload) => {
    return ({
        type: SET_SPH,
        payload
    })
};
export const setCityCode = (payload) => {
    return ({
        type: SET_CITY_CODE,
        payload
    })
};