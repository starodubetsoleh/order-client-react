import { GET_BRANDS, SET_BRAND, GET_MARKS, SET_MARK, SET_MARK_CODE, SET_SPH, GET_CITY, SET_CITY_CODE, GET_TABLE_DATA } from '../actions/actions'

const brandReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case GET_BRANDS: {
            return {
                ...state,
                brands: payload
            }
        }
        case SET_BRAND: {
            return {
                ...state,
                selectedBrand: payload
            }
        }
        case GET_MARKS: {
            return {
                ...state,
                marks: payload
            }
        }
        case GET_CITY: {
            return {
                ...state,
                city: payload
            }
        }
        case SET_MARK: {
            return {
                ...state,
                selectedMark: payload
            }
        }
        case SET_MARK_CODE: {
            return {
                ...state,
                selectedMarkCode: payload
            }
        }
        case SET_SPH: {
            return {
                ...state,
                selectedSph: payload
            }
        }
        case SET_CITY_CODE: {
            return {
                ...state,
                selectedCityCode: payload
            }
        }
        case GET_TABLE_DATA: {
            return {
                ...state,
                tableData: payload,
            }
        }
        default:
            return state
    }
}

export default brandReducer;